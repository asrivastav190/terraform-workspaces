# Terraform Workspaces
## Domain: Terraform
### Service: Terraform Workspaces
#### Description
This section deploys all terraform module resources.

\---terraform-workspaces
    \---workspace
        +---images
        +---infrastructure-aft
        |   +---images
        |   \---root
        \---infrastructure-network
            +---root
            |   \---templates
            \---security-root


Workspace is divided into 2 sections:
> 1.)Infrastructure-aft

> 2.)Infrastructure-network


##### Infrastructure-network

This section is responsible for calling all modules under the terraform-module folder and deploying its resources.

This is in turn divided into 2 more sections:
> Root

> Security-root

\---infrastructure-network
            |   .gitignore
            |
            +---root
            |   |   data.tf
            |   |   main.tf
            |   |   outputs.tf
            |   |   providers.tf
            |   |   variables.tf
            |   |   versions.tf
            |   |
            |   \---templates
            |           backup-policy.json
            |           bucket-policy.json
            |
            \---security-root
                    main.tf
                    outputs.tf
                    providers.tf
                    variables.tf


##### Root
Below is the reference screenshot:

  +---root
            |   |   data.tf
            |   |   main.tf
            |   |   outputs.tf
            |   |   providers.tf
            |   |   variables.tf
            |   |   versions.tf
            |   |
            |   \---templates
            |           backup-policy.json
            |           bucket-policy.json



The functionality of these files have been specifically covered in each terraform module section.

###### Commands to Execute

**terraform plan**
 1.)	Login to terraform cloud and select terraform-workspace with below VCS configuration

![Image](workspace/images/root_vcs.png)

2.)	Click on “Start new plan” 

![Image](workspace/images/startplan.png)

3.)	You will be prompted with the below window.

![Image](workspace/images/plan.png)

4.)	Click “Start plan”

![Image](workspace/images/planning.png)


**terraform apply**
1.)	Click on “Confirm & Apply” as shown in the below screenshot and then select confirm plan.

![Image](workspace/images/apply.png)

The above command will deploy  Workload/Spoke VPC, AWS Inspection VPC ,AWS Ingress VPC, AWS Egress VPC,AWS S3 for VPC FlowLog,Logging ,Gateway Endpoint , SCP,Organization Backup Policy, Backup Vault, AWS Transit Gateway and AWS Workload VPC with Public Subnet and AWS Network Firewall

#### Reference Links:

[url] https://www.terraform.io/cloud-docs/run/manage



#### Security-root

          \---security-root
                    main.tf
                    outputs.tf
                    providers.tf
                    variables.tf


This section is responsible for deploying in the security group, NACL for all VPCs and network firewall for inspection and public/private workload VPCs.

Create separate workspace with below VCS configuration:

![Image](workspace/images/sr_vcs.png)


To do so, first make a note of below details by getting terraform output  in root folder ( https://support.hashicorp.com/hc/en-us/articles/360001944768-Retrieving-Run-Outputs-from-Terraform-Cloud ) or could check on terraform cloud as shown in below screenshot:

![Image](workspace/images/output.png)

>Inspection VPC ID & its public & private subnet IDs
>Ingress VPC ID & its public & private subnet IDs
>Egress VPC ID & its public & private subnet IDs
>Inspection VPC network subnet IDs
>Any one Inspection VPC transit gateway subnet ID

Commands to Execute
Once the above details have been collected and added these to variables.tf file, run the below commands:

> terraform plan

![Image](workspace/images/sr_plan.png)

> terraform apply

![Image](workspace/images/sr_apply.png)
