# # # ##output from ou_policies module
# # #SCP
output "root_id" {
  value = module.scp.root_id
}

output "deny_marketplace_policy_id" {
  value = module.scp.deny_marketplace_policy_id
}

output "scp_policy_account_id" {
  value = module.scp.scp_policy_account_id
}

output "sso_arn" {
  value = module.scp.sso_arn
}

# # #tag
output "tag_policy_id" {
  value = module.tag_policy.tag_policy_id
}

output "tag_policy_account_id" {
  value = module.tag_policy.tag_policy_account_id
}
