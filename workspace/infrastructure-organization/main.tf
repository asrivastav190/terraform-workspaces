# # ## Organization Policies

module "scp" {
  providers = {
    aws.pwc_explore = aws.pwc_explore
  }
  source                = "bitbucket.org/iso-ne/terraform-modules/ou_policies/service_control_policy"
  marketplace_rule_name = var.marketplace_rule_name
  sandbox_account_id    = var.sandbox_account_id
  group_name            = var.group_name
  sso_perms_name        = var.sso_perms_name
}

module "tag_policy" {
  providers = {
    aws.pwc_explore = aws.pwc_explore
  }
  source                        = "bitbucket.org/iso-ne/terraform-modules/ou_policies/tag_policy"
  tag_policy_name               = var.tag_policy_name
  iso_deployment_artifact_value = var.iso_deployment_artifact_value
  iso_environment_value         = var.iso_environment_value
  iso_contact_value             = var.iso_contact_value
  iso_activity_code             = var.iso_activity_code
  name                          = var.name
  tags                          = var.tags
}