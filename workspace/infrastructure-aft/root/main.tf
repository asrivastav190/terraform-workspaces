module "aft" {
  source                      = "github.com/aws-ia/terraform-aws-control_tower_account_factory?ref=v1.0.11"
  ct_management_account_id    = var.ct_management_account_id
  log_archive_account_id      = var.log_archive_account_id
  audit_account_id            = var.audit_account_id
  aft_management_account_id   = var.aft_management_account_id
  ct_home_region              = var.ct_home_region
  tf_backend_secondary_region = var.tf_backend_secondary_region


  # VCS Vars
  vcs_provider                                  = "bitbucket"
  account_request_repo_name                     = "iso-ne/aft-account-request"
  global_customizations_repo_name               = "iso-ne/aft-global-customization"
  account_customizations_repo_name              = "iso-ne/aft-account-customiztaion"
  account_provisioning_customizations_repo_name = "iso-ne/aft-account-provisioning-customization"

  #TF Vars
  terraform_distribution = "tfc"
  terraform_token        = "Enter your terraform cloud token here"
  terraform_org_name     = "Enter Your terraform coud organization name here"
}