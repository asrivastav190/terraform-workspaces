Description
-----------

AWS Control Tower Account Factory for Terraform (AFT) is a Terraform module that makes it easy to create and customize new accounts that comply with your organization's security guidelines. AFT defines a pipeline for automated and consistent creation of AWS Control Tower accounts, giving you the benefits of Terraform’s workflow and Control Tower's governance features. AWS maintains this module.

Prerequisites
--------------
-Terraform Cloud access with terraform cloud token (User Token)
-An AWS account, with credentials for a non-root user with the Administrator Access policy attached. Some steps can take up to 30 minutes, so make sure your credentials have a long enough duration.
-AWS Control Tower enabled, with the default Log and Audit accounts created
-The AWS CLI.
-Bitbucket account.
-Terraform cloud to Bitbucket connection. 

Pre-Deployment Steps
---------------------
Create a AWS AFT organizational unit and account (Recommended but not Mandatory)
---------------------------------------------------------------------------------

In the AWS dashboard, navigate to Control Tower, then select Organizational Units (OU) in the left sidebar. 

Click Add an OU to create a new organizational unit for AFT.

![Image1](../images/image1.PNG)

Name the OU AFT and select your Root OU as the parent OU, then click Add.

![Image2](../images/image2.PNG)

Once your new OU is ready, navigate to Account Factory in the left sidebar, then click Enroll Account.

![Image3](../images/image3.PNG)

Enter an email address not associated with existing AWS accounts in the Account email field. This will be the account's root email address. Enter learn-aft for the display name; enter an email address you have access to for the SSO email, and enter Learn and AFT for the AWS SSO user’s first and last name. Then, set AFT as the Parent OU. Finally, click Enroll Account.

![Image4](../images/image4.PNG)

*It can take 20 to 30 minutes to provision a new account in Control Tower..

This solution will primarily reference two AWS accounts:

-The Control Tower management account, in which you launched your Control Tower landing zone, and
-The AFT management account, which you will provision in this section. The AFT module creates most of its resources in this account.

Create and Populate the repos in Bitbucket with the given structure:
----------------------------------------------------------------------

In this step, we need to create below four repos in Bitbucket and populate them with the given structure.

-aft-account-request
-aft-global-customization
-aft-account-customization
-aft-account-provisioning-customization

AFT Module Deployment:
----------------------

AWS maintains the control_tower_account_factory Terraform module. This module defines a pipeline of AWS services that allow you to provision and customize accounts in Control Tower. In this section, you will deploy the module and review its services and resources.
Update AFT module configuration

Open the main.tf file in your code editor to review and modify the module configuration. This file is the minimal configuration for deploying AFT

This module provisions resources across the Log, Audit, Control Tower Management, and AFT management accounts in your Landing Zone. Navigate to the Control Tower dashboard to access the respective account IDs and update the configuration.

Input variable           | Account name (assuming defaults used)
------------------------ |--------------------------------------
ct_management_account_id |
Your root account ID     |
log_archive_account_id   |
Log Archive              |
audit_account_id         |
Audit                    |
aft_management_account_id|
AFT Management Account ID|

Enter below input variables from bitbucket repos created.

vcs_provider                                 | "bitbucket"
account_request_repo_name                    | ExampleOrg/example-repo-1
global_customizations_repo_name              | ExampleOrg/example-repo-2
account_customizations_repo_name             | ExampleOrg/example-repo-3
account_provisioning_customizations_repo_name| ExampleOrg/example-repo-4

Enter below Terraform cloud variables from Terraform cloud configuration.

terraform_distribution  | "tfc"
terraform_token         | “Your terraform cloud token, use user token not org token”
terraform_org_name      | “Your terraform organization name”


For the ct_home_region input variable, use the same region as the one Control Tower is enabled in. Verify the region in the top right hand corner of the AWS console. You must deploy the module in the same region as your Control Tower enablement.

AFT creates a backend to store Terraform state. The primary backend region is the same as the region you enabled Control Tower in. It also replicates the data to a secondary region. Set the tf_backend_secondary_region variable to the replication region of your choice

Getting a specific version of the AFT Module.
In the main.tf in AFT module, you will need to refer to a specific version as shown below. We can use version 1.0.11 to deploy AFT as it’s tested and stable.

Create a Terraform Cloud workspace:
------------------------------------

Step 1: Create terraform cloud workspace named “aft-deployment”. Choose type “version control workflow”

![Image5](../images/image5.PNG)

Step 2: Connect to VCS BitBucket Cloud.

![Image6](../images/image6.PNG)

Step 3: Select the repo that hosts the AFT module

![Image7](../images/image7.PNG)

In your environment it will be “iso-ne/terraform-workspaces”

Step 4: In configure settings, expand the Advanced setting section and specify the path for the AFT main.tf file, as shown below.

![Image8](../images/image8.PNG)

Once the connection is established, go to the Terraform Cloud workspace and apply the Terraform configuration. It will deploy 300+ resources and will take 30-40 minutes for complete deployment.

Post Deployment Steps:
----------------------

-Enable the Code Star Connection
-Grant AFT Access to the Service Catalog Portfolio
-Re-run the Account Provisioning Pipeline

Enable Code Star Connection
----------------------------
The AFT module sets up a CodeStar connection to watch for changes to your Github account request and customization repositories. In this section, you will approve the CodeStar connection to enable the service.

Once Terraform completes deploying the AFT module, log in to your AFT management account using the email you specified in its configuration earlier.

Then, navigate to your CodeSuite connections in the AWS Console.
Click on the ct-aft-github-connection connection, which is still pending

![Image9](../images/image9.PNG)

On the details page, click Update pending connection.

![Image10](../images/image10.PNG)

Grant AFT access to thee Service Catalog portfolio 
---------------------------------------------------

Although AFT runs in your AFT management account, it needs access to your Control Tower management account in order to fulfill your account provisioning requests. To enable access, you must add the AWSAFTExecution IAM role created by the pipeline to Control Tower’s Service Catalog portfolio.
Log into the Control Tower management account in the AWS console. This is the account in which you enabled Control Tower and manually created the learn-aft account at the beginning of this tutorial.
Then, navigate to Portfolios in the Service Catalog page.
Click on the AWS Control Tower Account Factory Portfolio.

![Image11](../images/image11.PNG)

Select the Groups, roles, and users tab, then click Add groups, roles, users.

![Image12](../images/image12.PNG)

Select the Roles tab, then search for AWSAFTExecution. Check the box next to it, and click Add access.

![Image13](../images/image13.PNG)

Re-run the account provisioning pipeline
-----------------------------------------

When you first deploy AFT, it attempts to run the account provisioning customizations pipeline but fails since you have not yet enabled the CodeStar connection. AWS automatically runs the pipeline on creation. You must now rerun the pipeline to create its downstream resources for when you are ready to provision an account using AFT.
In your AFT management account, navigate to your CodePipeline page. Select the ct-aft-account-provisioning-customizations pipeline, then click Release change. Then, click Release to rerun the pipeline.

![Image14](../images/image14.PNG)

Once all above steps are completed, AFT is ready to vend new customized accounts.

Creating & Customizing a New Account
-------------------------------------

Edit and Push New Account Requests:

Place the account request file in the aft-account-request-repo as shown below-

![Image15](../images/image15.PNG)

Apply Global Customization:

Global customization will be applied to all accounts that we are creating in a single request. Place the terraform configuration file as shown below-

![Image16](../images/image16.PNG)

In the above example we are creating a backup vault in all the accounts that will be created using AFT.

Apply Account Customization: 

Any account specific customization needs to be placed in the aft-account-customization repo and the same directory name needs to be passed in the account request file in a parameter named account_customizations_name.

In the above example we are creating a S3 bucket.

![Image17](../images/image17.PNG)

Now push all these changes to the repos in the below sequence.

-Push changes to the aft-global-customization repo.
-Push changes to the aft-account-customization repo.
-Push changes to the aft-account-request repo.

As soon as changes are pushed to aft-account-request repo it will trigger AWS CodePipeline cft-aft-account-request. It will take 10-15 minutes for new account creation and enrollment and customization. We can verify the account in the Service Catalog and Control Tower.
