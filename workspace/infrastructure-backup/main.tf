# # ##backup Vault

module "backup_vault" {
  providers = {
    aws = aws.use1
  }
  source       = "bitbucket.org/iso-ne/terraform-modules/backup_vault"
  vault_name   = var.vault_name
  tags         = var.tags
  enable_vault = var.enable_vault
}


# # ## Organization backup policy
module "backup_policy" {
  providers = {
    aws.pwc_explore = aws.pwc_explore
  }
  source                 = "bitbucket.org/iso-ne/terraform-modules/ou_policies/backup_policy"
  backup_policy_name     = var.backup_policy_name
  backup_policy_rendered = data.template_file.backup_policy.rendered
  role_arn               = var.role_arn
  tags                   = var.tags
}