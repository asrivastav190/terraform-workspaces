## Template file for backup policy
data "template_file" "backup_policy" {
  template = file("${path.module}/templates/backup-policy.json")
  vars = {
    target_backup_vault_name                        = var.target_backup_vault_name
    backup_plan_name                                = var.backup_plan_name
    start_backup_window_minutes                     = var.start_backup_window_minutes
    complete_backup_window_minutes                  = var.complete_backup_window_minutes
    move_to_cold_storage_after_days                 = var.move_to_cold_storage_after_days
    delete_after_days                               = var.delete_after_days
    tag_key                                         = var.tag_key
    tag_value                                       = var.tag_value
    windows_vss                                     = var.windows_vss
    secondary_vault_move_to_cold_storage_after_days = var.secondary_vault_move_to_cold_storage_after_days
    secondary_vault_delete_after_days               = var.secondary_vault_delete_after_days
    tertiary_vault_move_to_cold_storage_after_days  = var.tertiary_vault_move_to_cold_storage_after_days
    tertiary_vault_delete_after_days                = var.tertiary_vault_delete_after_days
    schedule_expression                             = var.schedule_expression
  }
}