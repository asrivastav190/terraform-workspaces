# # #output from backup_vault module

# # # Vault
output "vault_id" {
  description = "The name of the vault"
  value       = module.backup_vault.vault_id
}

output "vault_arn" {
  description = "The ARN of the vault"
  value       = module.backup_vault.vault_arn
}

# #backup_policy
output "backup_policy_id" {
  value = module.backup_policy.backup_policy_id
}

output "backup_policy_account_id" {
  value = module.backup_policy.backup_policy_account_id
}