variable "role_arn" {}
# ##Variables for backup Vault

variable "enable_vault" {
  description = "Select option to enable/disable vault"
  type        = bool
  default     = true
}

variable "vault_name" {
  type    = string
  default = "iso-backup-04"
}

# ##Variables for backuppolicy

variable "backup_policy_name" {
  type    = string
  default = "backup_policy_005"
}

variable "target_backup_vault_name" {
  type    = string
  default = "iso004"
}

variable "backup_plan_name" {
  type    = string
  default = "new-backup-plan"
}

variable "start_backup_window_minutes" {
  type    = number
  default = 490
}

variable "complete_backup_window_minutes" {
  type    = number
  default = 10090
}

variable "move_to_cold_storage_after_days" {
  type    = number
  default = 190
}
variable "delete_after_days" {
  type    = number
  default = 280
}

variable "secondary_vault_move_to_cold_storage_after_days" {
  type    = number
  default = 40
}

variable "secondary_vault_delete_after_days" {
  type    = number
  default = 100
}

variable "tertiary_vault_move_to_cold_storage_after_days" {
  type    = number
  default = 40
}

variable "tertiary_vault_delete_after_days" {
  type    = number
  default = 100
}

variable "tag_key" {
  type    = string
  default = "ENV"
}

variable "tag_value" {
  type    = string
  default = "PROD"
}

variable "windows_vss" {
  type    = string
  default = "disabled"
}

variable "schedule_expression" {
  default = "cron(0 1 ? * * *)"
}

## Tags
variable "tags" {
  type = map(string)
  default = { "iso-environment" = "sandbox", "iso-contact" = "eg@iso-ne.com", "iso-deployment-artifact" = "cmdb" , "iso-activity-code" = "1234"}
}