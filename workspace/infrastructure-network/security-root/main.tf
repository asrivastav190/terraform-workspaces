# #Create NACL and AWS network firewall.

module "private_nacl_sg_rules" {
  providers = {
    aws = aws.use1
  }
  source             = "bitbucket.org/iso-ne/terraform-modules/security-rules/nacl-sg/prv-nacl-sg"
  for_each           = var.vpc_prv_subnets
  vpc_id             = each.value["vpc_id"]
  private_subnets    = each.value["private_subnet_ids"]
  vpc_name           = each.value["vpc_name"]
  private_nacl_rules = var.private_nacl_rules
  ingress_rules      = var.all_ingress_rules
  egress_rules       = var.all_egress_rules
  tags               = var.tags
}

module "public_nacl_rules" {
  providers = {
    aws = aws.use1
  }
  source            = "bitbucket.org/iso-ne/terraform-modules/security-rules/nacl-sg/pub-nacl"
  for_each          = var.vpc_pub_subnets
  vpc_id            = each.value["vpc_id"]
  public_subnets    = each.value["public_subnet_ids"]
  vpc_name          = each.value["vpc_name"]
  public_nacl_rules = var.public_nacl_rules
  tags              = var.tags
}

## Create newtork firewall for Inspection VPC.
module "inspection_network_firewall" {
  providers = {
    aws = aws.use1
  }
  source               = "bitbucket.org/iso-ne/terraform-modules/security-rules/inspection-network-firewall"
  vpc_id               = var.inspection_vpc_id
  nfw_subnet           = var.prv_nfw_subnet_ids
  inspection_env       = var.inspection_env
  inspection_vpc_cidr  = var.inspection_vpc_cidr
  stateless_rule_group = var.stateless_rule_group
  stateful_rule_group  = var.stateful_rule_group
  subnet_id            = var.prv_tgw_subnet_id
  tags                 = var.tags
}

## Create newtork firewall for public/private workload VPC.
module "public_workload_vpc_nfw" {
  providers = {
    aws = aws.use1
  }
  for_each             = var.pub_vpc_ids
  source               = "bitbucket.org/iso-ne/terraform-modules/security-rules/pub-prv-workload-vpc-nfw"
  vpc_id               = each.value["vpc_id"]
  pub_vpce_rt_id       = each.value["pub_vpce_rt_id"]
  vpc_name             = each.value["vpc_name"]
  pub_sub_suffix       = var.pub_sub_suffix
  nfw_sub_suffix       = var.nfw_sub_suffix
  prv_share_sub_suffix = var.prv_share_sub_suffix
  az                   = var.az
  stateless_rule_group = var.stateless_rule_group
  stateful_rule_group  = var.stateful_rule_group
  tags                 = var.tags
}