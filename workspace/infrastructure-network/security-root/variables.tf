
## Tags
variable "tags" {
  type    = map(string)
  default = { "iso-environment" = "sandbox", "iso-contact" = "eg@iso-ne.com", "iso-deployment-artifact" = "cmdb", "iso-activity-code" = "1234" }
}


## Variables declared for private_nacl_sg_rules module.

variable "private_nacl_rules" {
  type = map(map(string))
  default = {
    "ssh_inbound"    = { "from_port" = 22, "to_port" = 22, "protocol" = "tcp", "rule_action" = "deny", "rule_number" = 200, "egress" = false, "cidr_block" = "0.0.0.0/0" },
    "http_inbound"   = { "from_port" = 80, "to_port" = 80, "protocol" = "tcp", "rule_action" = "deny", "rule_number" = 300, "egress" = false, "cidr_block" = "0.0.0.0/0" },
    "https_inbound"  = { "from_port" = 443, "to_port" = 443, "protocol" = "tcp", "rule_action" = "deny", "rule_number" = 400, "egress" = false, "cidr_block" = "0.0.0.0/0" },
    "ssh_outbound"   = { "from_port" = 22, "to_port" = 22, "protocol" = "tcp", "rule_action" = "allow", "rule_number" = 500, "egress" = true, "cidr_block" = "0.0.0.0/0" },
    "http_outbound"  = { "from_port" = 80, "to_port" = 80, "protocol" = "tcp", "rule_action" = "allow", "rule_number" = 600, "egress" = true, "cidr_block" = "0.0.0.0/0" },
    "https_outbound" = { "from_port" = 443, "to_port" = 443, "protocol" = "tcp", "rule_action" = "allow", "rule_number" = 700, "egress" = false, "cidr_block" = "0.0.0.0/0", }
    #  "all_traffic_inbound"  = { "from_port" = 0, "to_port" = 0, "protocol" = "-1", "rule_action" = "allow", "rule_number" = 200, "egress" = false, "cidr_block" = "0.0.0.0/0" },
    #  "all_traffic_outbound" = { "from_port" = 0, "to_port" = 0, "protocol" = "-1", "rule_action" = "allow", "rule_number" = 200, "egress" = true, "cidr_block" = "0.0.0.0/0" }
  }
}
## Security group rules
variable "all_ingress_rules" {
  type = map(map(string))
  default = {
    "all_traffic" = { "type" = "ingress", "from_port" = 0, "to_port" = 0, "protocol" = "-1", "description" = "Allow all ingress", cidr_blocks = "0.0.0.0/0", ipv6_cidr_blocks = "::/0", self = null, security_groups = "", source_security_group_id = null },
    "http"        = { "type" = "ingress", "from_port" = 80, "to_port" = 80, "protocol" = "tcp", "description" = "HTTP from Internet", cidr_blocks = "0.0.0.0/0", ipv6_cidr_blocks = "::/0", self = null, security_groups = "", source_security_group_id = null },
    "http"        = { "type" = "ingress", "from_port" = 443, "to_port" = 443, "protocol" = "tcp", "description" = "HTTPS from Internet", cidr_blocks = "0.0.0.0/0", ipv6_cidr_blocks = "::/0", self = null, security_groups = "", source_security_group_id = null }
  }
}
variable "all_egress_rules" {
  type = map(map(string))
  default = {
    "all_traffic_inbound" = { "type" = "egress", "from_port" = 0, "to_port" = 0, "protocol" = "-1", "description" = "Allow all egress", cidr_blocks = "0.0.0.0/0", ipv6_cidr_blocks = "::/0", self = null, security_groups = "", source_security_group_id = null },
  }
}

variable "vpc_prv_subnets" {
  type = map(object({
    vpc_name           = string
    vpc_id             = string
    private_subnet_ids = list(string)
  }))
  default = {
    pub_production_vpc = {
      vpc_id             = "vpc-0751fb18a972e741d"
      private_subnet_ids = ["subnet-05061ae2450d71969","subnet-05b29592cb0f6c765","subnet-059123ef31cebd0c2","subnet-06e1f8f4a10da864c"]
      vpc_name           = "public_production_vpc"
    },
    pub_development_vpc = {
      vpc_id             = "vpc-0d6093e2f3e4fa6db"
      private_subnet_ids = ["subnet-05031602c4cdb1562","subnet-0b5b0c32ac9bd0fad","subnet-0e3e3218e79a61865","subnet-015fa267910511406"]
      vpc_name           = "public_development_vpc"
    }
    prv_production_vpc = {
      vpc_id             = "vpc-0412da387f321de42"
      private_subnet_ids = ["subnet-08bfb87b80b01da3d","subnet-0097a5fdac1441d31","subnet-04f23216d75c43ec7","subnet-01319b31a8cf05ec1"]
      vpc_name           = "production_vpc"
    },
    prv_development_vpc = {
      vpc_id             = "vpc-0cb22f7dfb4ba5095"
      private_subnet_ids = ["subnet-00d9f22139738d41a","subnet-0a5c17d4e371c1dcf","subnet-0611bb67197967fcd","subnet-0a1de50feda441b16"]
      vpc_name           = "development_vpc"
    }
    # prv_integration_vpc = {
    #   vpc_id             = "vpc-047bbb498ec8ce0b9"
    #   private_subnet_ids = ["subnet-078531edebb1fa3bc","subnet-0ba9059d9c1a36ac0","subnet-02de6c16735c36c7c","subnet-0b498181ac886a238"]
    #   vpc_name           = "integration_vpc"
    # }
    # prv_research_vpc = {
    #   vpc_id             = "vpc-0412da387f321de42"
    #   private_subnet_ids = ["subnet-0d7e81d24f756e775","subnet-0d94bf1f6786b8c62","subnet-0cb06d98b071613aa","subnet-0037fec113274d070"]
    #   vpc_name           = "research_vpc"
    # }

    inspection_vpc = {
      vpc_id             = "vpc-0265266da389d0041"
      private_subnet_ids = ["subnet-0f7b57f50677a5022","subnet-0d720399d795ae36c","subnet-019320a854b2de65a","subnet-0480c8aded87b2533"]
      vpc_name           = "inspection_vpc"
    },
    ingress_vpc = {
      vpc_id             = "vpc-0d61208b99ffd7608"
      private_subnet_ids = ["subnet-0b64c8747b132f45e","subnet-02e8f042c476f8380"]
      vpc_name           = "ingress_vpc"
    }
    egress_vpc = {
      vpc_id             = "vpc-06cf39377abf52722"
      private_subnet_ids = ["subnet-0e8941c0368bc2028","subnet-0711a1ad6c520b406"]
      vpc_name           = "egress_vpc"
    }

  }
}

## Variables declared for public_nacl_rules module.
variable "public_nacl_rules" {
  type = map(map(string))
  default = {
    "inbound_traffic"  = { "from_port" = 0, "to_port" = 0, "protocol" = "-1", "rule_action" = "deny", "rule_number" = 100, "egress" = false, "cidr_block" = "0.0.0.0/0" },
    "outbound_traffic" = { "from_port" = 0, "to_port" = 0, "protocol" = "-1", "rule_action" = "allow", "rule_number" = 200, "egress" = true, "cidr_block" = "0.0.0.0/0" },
    #  "all_traffic_inbound"  = { "from_port" = 0, "to_port" = 0, "protocol" = "-1", "rule_action" = "allow", "rule_number" = 100, "egress" = false, "cidr_block" = "0.0.0.0/0" },
    #  "all_traffic_outbound" = { "from_port" = 0, "to_port" = 0, "protocol" = "-1", "rule_action" = "allow", "rule_number" = 200, "egress" = true, "cidr_block" = "0.0.0.0/0" }
  }
}

variable "vpc_pub_subnets" {
  type = map(object({
    vpc_name          = string
    vpc_id            = string
    public_subnet_ids = list(string)
  }))
  default = {
    production_vpc = {
      vpc_id             = "vpc-0751fb18a972e741d"
      public_subnet_ids = ["subnet-092784399654a24f1","subnet-0c0e4ab62cea61631","subnet-0fb838fcf006c964a","subnet-07474a057abe6a5b4"]
      vpc_name           = "public_production_vpc"
    }
    development_vpc = {
      vpc_id             = "vpc-0d6093e2f3e4fa6db"
      public_subnet_ids = ["subnet-0b9dd1890c4c67be0","subnet-0c13dd369825365ef","subnet-028851adefeed25a7","subnet-0873bba8ec136f5e7"]
      vpc_name           = "public_development_vpc"
    }
    ingress_vpc = {
      vpc_id            = "vpc-0d61208b99ffd7608"
      public_subnet_ids = ["subnet-0fe5bd1bfcf06f39e","subnet-09b7d120ec6809170"]
      vpc_name          = "ingress_vpc"
    }
    egress_vpc = {
      vpc_id            = "vpc-06cf39377abf52722"
      public_subnet_ids = ["subnet-0319ab400bc293dc1","subnet-07ca0000337d8b7b8"]
      vpc_name          = "egress_vpc"
    }

  }
}

# ## Variable declared for inspection_network_firewall module & public_workload_vpc_nfw module.
variable "inspection_vpc_id" {
  default = "vpc-0265266da389d0041"
}

variable "prv_nfw_subnet_ids" {
  default = ["subnet-0f7b57f50677a5022","subnet-0d720399d795ae36c"]
}

variable "prv_tgw_subnet_id" {
  default = "subnet-019320a854b2de65a"
}

variable "pub_vpc_ids" {
  type = map(object({
    vpc_id         = string
    vpc_name       = string
    pub_vpce_rt_id = string
  }))
  default = {
    production_vpc = {
      vpc_id         = "vpc-0751fb18a972e741d"
      vpc_name       = "ProductionVPC"
      pub_vpce_rt_id = "rtb-0ec9eead0d5847416"
    },
    development_vpc = {
      vpc_id         = "vpc-0d6093e2f3e4fa6db"
      vpc_name       = "DevelopmentVPC"
      pub_vpce_rt_id = "rtb-0caeb4ff4b528ffc8"
    }
    # research_vpc = {
    # vpc_id = ""
    # vpc_name = "public_research_vpc"
    # pub_vpce_rt_id = ""
    # },
    # integration_vpc = {
    # vpc_id = ""
    # vpc_name = "public_integration_vpc"
    # pub_vpce_rt_id = ""
    # }
  }
}

variable "inspection_env" {
  description = "Environment name"
  default     = "InspectionVPC"
}

variable "stateless_rule_group" {
  type        = map(map(string))
  description = "Set of Stateless Rule Groups"
  default = {
    "ephemeral-port-rule" : { "capacity" : 100, "priority" : 1, "actions" : "aws:drop", "source_from_port" : 1024, "source_to_port" : 65535, "protocol" : 6, "source_address" : "0.0.0.0/0", "destination_from_port" : 1024, "destination_to_port" : 65535, "destination_address" : "0.0.0.0/0" },
  }
}

variable "stateful_rule_group" {
  type        = map(map(string))
  description = "Set of Stateful Rule Groups"
  default = {
    "drop-icmp" : { "capacity" : 100, "action" : "DROP", "destination" : "0.0.0.0/0", "destination_port" : 1, "direction" : "ANY", "protocol" : "TCP", "source" : "0.0.0.0/0", "source_port" : 1 },
    "ssh-rule" : { "capacity" : 100, "action" : "DROP", "destination" : "0.0.0.0/0", "destination_port" : 22, "direction" : "ANY", "protocol" : "TCP", "source" : "0.0.0.0/0", "source_port" : 22 },
    "http-rule" : { "capacity" : 100, "action" : "DROP", "destination" : "0.0.0.0/0", "destination_port" : 80, "direction" : "ANY", "protocol" : "TCP", "source" : "0.0.0.0/0", "source_port" : 80 },
    "https-rule" : { "capacity" : 100, "action" : "DROP", "destination" : "0.0.0.0/0", "destination_port" : 443, "direction" : "ANY", "protocol" : "TCP", "source" : "0.0.0.0/0", "source_port" : 443 },
  }
}


variable "inspection_vpc_cidr" {
  description = "Inspection VPC CIDR range"
  default     = "10.161.0.0/16"
}

variable "az" {
  description = "List of Availability Zones to be initialized in Workload VPC"
  type        = set(string)
  default     = ["a", "b"]
}

variable "pub_sub_suffix" { default = "SN-Pub-Share" }
variable "nfw_sub_suffix" { default = "Pub-NFW" }
variable "prv_share_sub_suffix" { default = "SN-Prv-Share" }  