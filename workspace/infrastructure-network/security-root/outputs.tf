##Output from nacl module.
output "private_nacl_id" {
  value       = values(module.private_nacl_sg_rules)[*].private_nacl_id
  description = "Private NACL"
}

output "sg_id" {
  value = values(module.private_nacl_sg_rules)[*].sg_id
}

output "public_nacl_id" {
  value       = values(module.public_nacl_rules)[*].public_nacl_id
  description = "Public NACL"
}

# # # ##output from module NetworkFirewall
output "inspection_firewall_id" {
  value       = module.inspection_network_firewall.firewall_id
  description = "AWS Network Firewall ID"
}

output "inspection_vpce_ids" {
  value       = module.inspection_network_firewall.vpce_ids
  description = "Firewall endpoint ID"
}


# ## public/private workload vpc
output "public_workload_firewall_id" {
  value = values(module.public_workload_vpc_nfw)[*].firewall_id
}
output "endpoints" {
  value = values(module.public_workload_vpc_nfw)[*].endpoint_id
}
