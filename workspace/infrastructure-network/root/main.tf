# ###Below Module will Create Transit Gateway in us-east-1 and us-west-2 region, route tables and peering attachment

module "create_transit_gateway_infra" {
  providers = {
    aws.tgw     = aws.use1
    aws.peertgw = aws.usw2
  }
  source                            = "git::https://bitbucket.org/asrivastav190/terraform-modules//transit-gateway"
  transit_gateway_name              = var.transit_gateway_name
  peer_transit_gateway_name         = var.peer_transit_gateway_name

  tags                              = var.tags
}

module "workload_vpc" {
  providers = {
    aws = aws.use1
  }
  for_each                                = var.spoke_vpc_param
  vpc_name                                = each.value["vpc_name"]
  cidr_ab                                 = each.value["cidr_ab"]
  flow_log_enable                         = each.value["flow_log_enable"]
  source                                  = "git::https://bitbucket.org/asrivastav190/terraform-modules//workload-vpc"
  map_public_ip_on_launch                 = var.map_public_ip_on_launch
  tgw_subnets                             = var.tgw_subnets
  s3_bucket_arn                           = module.create_s3.s3_bucket_arn
  tags                                    = var.tags
}

#creates a set of subnets for private workload vpc.
module "workload_vpc_prv_subnets" {
  providers = {
    aws = aws.use1
  }
  for_each             = tomap(zipmap(keys(module.workload_vpc)[*], values(module.workload_vpc)[*].vpc_id))
  source               = "git::https://bitbucket.org/asrivastav190/terraform-modules//workload-vpc/subnets"
  map_public_ip_on_launch = var.map_public_ip_on_launch
  vpc_id               = each.value
  subnets              = var.prv_subnets
  subnet_name          = "SN-Prv"
  vpc_name             = each.key
  tags                 = var.tags
  depends_on           = [module.workload_vpc]
}

# module "workload_vpc_workload_subnets" {
#   providers = {
#     aws = aws.use1
#   }
#   for_each             = tomap(zipmap(keys(module.workload_vpc)[*], values(module.workload_vpc)[*].vpc_id))
#   source               = "git::https://bitbucket.org/asrivastav190/terraform-modules//workload-vpc/subnets"
#   map_public_ip_on_launch = var.map_public_ip_on_launch
#   vpc_id               = each.value
#   subnets              = var.workload_subnets
#   subnet_name          = "App1-Private"
#   vpc_name             = each.key
#   tags                 = var.tags
#   depends_on           = [module.workload_vpc]
# }

module "workload_vpc_route_table" {
  providers = {
    aws = aws.use1
  }
  for_each             = tomap(zipmap(keys(module.workload_vpc)[*], values(module.workload_vpc)[*].vpc_id))
  source               = "git::https://bitbucket.org/asrivastav190/terraform-modules//workload-vpc/route-table"
  transit_gateway_id   = module.create_transit_gateway_infra.transit_gateway_id
  vpc_id               = each.value
  vpc_name             = each.key
  subnet_ids           = concat(["module.workload_vpc.tgw_subnet_ids"],["module.workload_vpc_prv_subnets.subnet_ids"])
  tags                 = var.tags
  depends_on           = [module.workload_vpc]
}

module "workload_vpc_tgw_attachment" {
  providers = {
    aws = aws.use1
  }
  for_each                                = tomap(zipmap(keys(module.workload_vpc)[*], values(module.workload_vpc)[*].vpc_id))
  source                                  = "git::https://bitbucket.org/asrivastav190/terraform-modules//workload-vpc/vpc-tgw-attachment"
  vpc_id                                  = each.value
  transit_gateway_id                      = module.create_transit_gateway_infra.transit_gateway_id
  spoke_vpc_inspection_tgw_route_table_id = module.create_transit_gateway_infra.spoke_vpc_inspection_tgw_route_table_id
  firewall_subnet_tgw_route_table_id      = module.create_transit_gateway_infra.firewall_subnet_tgw_route_table_id
}

module "inspection_ingress_egress_vpcs" {
  providers = {
    aws = aws.use1
  }
  source                                  = "git::https://bitbucket.org/asrivastav190/terraform-modules//inspection-ingress-egress-vpc"
  for_each                                = var.inspection_ingress_egress_param
  map_public_ip_on_launch                 = var.map_public_ip_on_launch
  cidr_ab                                 = each.value["cidr_ab"]
  flow_log_enable                         = each.value["flow_log_enable"]
  vpc_name                                = each.value["vpc_name"]
  s3_bucket_arn                           = module.create_s3.s3_bucket_arn
  inspection_ingress_egress_tgw_subnets   = var.inspection_ingress_egress_tgw_subnets
  inspection_nfw_subnets  = var.inspection_nfw_subnets
  tags                                    = var.tags
  depends_on                              = [module.workload_vpc]
}

module "ingress_egress_pub_subnets" {
  providers = {
    aws = aws.use1
  }
  for_each                = zipmap(local.ingress_egress_vpcs_name,local.ingress_egress_vpcs_id)

  source                  = "git::https://bitbucket.org/asrivastav190/terraform-modules//inspection-ingress-egress-vpc/subnets"
  map_public_ip_on_launch = var.map_public_ip_on_launch
  vpc_id               = each.value
  subnets              = var.ingress_egress_pub_subnets
  # inspection_vpc_id    = join(",",local.inspection_vpc_id)
  # inspection_nfw_subnets  = var.inspection_nfw_subnets
  subnet_name          = format("%s-pub-subnet",each.key)
  tags                 = var.tags
}

module "inspection_ingress_egress_route_tables" {
  providers = {
    aws = aws.use1
  }
  for_each             = tomap(local.inspection_ingress_egress_vpc_name_id_map)
  source               = "git::https://bitbucket.org/asrivastav190/terraform-modules//inspection-ingress-egress-vpc/route-table"
  transit_gateway_id   = module.create_transit_gateway_infra.transit_gateway_id
  vpc_id               = each.value
  env                  = each.key
  tgw_count            = keys(var.inspection_ingress_egress_tgw_subnets)
  nfw_count            = keys(var.inspection_nfw_subnets)
  pub_count            = keys(var.ingress_egress_pub_subnets)
  tags                 = var.tags
}

module "inspection_ingress_egress_vpc_tgw_attachment" {
  providers = {
    aws = aws.use1
  }
  source                                  = "git::https://bitbucket.org/asrivastav190/terraform-modules//inspection-ingress-egress-vpc/vpc-tgw-attachment"
  inspection_vpc_id                       = join(",",local.inspection_vpc_id)
  ingress_vpc_id                          = join(",",local.ingress_vpc_id)
  egress_vpc_id                           = join(",",local.egress_vpc_id)
  transit_gateway_id                      = module.create_transit_gateway_infra.transit_gateway_id
  spoke_vpc_inspection_tgw_route_table_id = module.create_transit_gateway_infra.spoke_vpc_inspection_tgw_route_table_id
  firewall_subnet_tgw_route_table_id      = module.create_transit_gateway_infra.firewall_subnet_tgw_route_table_id
}

# # ## S3 MODULE
module "create_s3" {
  source                             = "git::https://bitbucket.org/asrivastav190/terraform-modules//s3"
  bucket                             = var.bucket
  log_bucket                         = var.log_bucket
  replica_log_bucket                 = var.replica_log_bucket
  aws_region                         = var.aws_region
  acl                                = var.acl
  sse_algorithm                      = var.sse_algorithm
  force_destroy                      = var.force_destroy # make this false later
  attach_policy                      = var.attach_policy # make this true later to attach bucket policy
  s3_policy_rendered                 = data.template_file.s3_bucket_policy.rendered
  app_env                            = var.app_env
  app_name                           = var.app_name
  noncurrent_version_transition_days = 600
  block_public_acls                  = var.block_public_acls
  block_public_policy                = var.block_public_policy
  ignore_public_acls                 = var.ignore_public_acls
  restrict_public_buckets            = var.restrict_public_buckets
  versioning                         = var.versioning
  tags                               = var.tags
}

# ### VPC S3 endpoints for internal connectivity for s3.

module "endpoint" {
  providers = {
    aws = aws.use1
  }
  #for_each           = var.endpoint_param
  #endpoint_name      = each.value["endpoint_name"]
  source               = "git::https://bitbucket.org/asrivastav190/terraform-modules//endpoints"
  app_env              = var.app_env
  app_name             = var.app_name
  vpc_id               = flatten([values(module.inspection_ingress_egress_vpcs)[*].vpc_id, values(module.workload_vpc)[*].vpc_id, values(module.public_workload_vpc)[*].vpc_id])
  enable_s3_endpoint   = var.enable_s3_endpoint
  private_route_tables = flatten([values(module.inspection_ingress_egress_route_tables)[*].prv_tgw_rt_ids, values(module.workload_vpc_route_table)[*].rt_id, values(module.public_workload_vpc_tgw_route_table)[*].rt_id])
  tags                 = var.tags
}

##Create public/private Workload VPC. This will create muliple VPC based on pub_prv_spoke_vpc_param varaible defintion.
# module "public_workload_vpc" {
#   providers = {
#     aws = aws.use1
#   }
#   for_each                                = var.pub_prv_spoke_vpc_param
#   source                                  = "git::https://bitbucket.org/asrivastav190/terraform-modules//public_workload_vpc/vpc"
#   vpc_name                                = each.value["vpc_name"]
#   vpc_cidr                                = each.value["vpc_cidr"]
#   public_flow_log_enable                  = each.value["public_flow_log_enable"]
#   s3_bucket_arn                           = module.create_s3.s3_bucket_arn
#   map_public_ip_on_launch                 = var.map_public_ip_on_launch
#   az_mapping                              = var.az_mapping
#   az_nums                                 = var.az_nums
#   az                                      = var.az
#   workload_account_id                     = var.workload_account_id
#   ram_name                                = var.ram_name
#   transit_gateway_id                      = module.create_transit_gateway_infra.transit_gateway_id
#   firewall_subnet_tgw_route_table_id      = module.create_transit_gateway_infra.firewall_subnet_tgw_route_table_id
#   spoke_vpc_inspection_tgw_route_table_id = module.create_transit_gateway_infra.spoke_vpc_inspection_tgw_route_table_id
#   tags                                    = var.tags
# }


module "public_workload_vpc" {
  providers = {
    aws = aws.use1
  }
  for_each                                = var.pub_prv_mspoke_vpc_param
  source                                  = "git::https://bitbucket.org/asrivastav190/terraform-modules//public_workload_vpc/vpc"
  vpc_name                                = each.value["vpc_name"]
  cidr_ab                                 = each.value["cidr_ab"]
  public_flow_log_enable                  = each.value["public_flow_log_enable"]
  map_public_ip_on_launch                 = var.map_public_ip_on_launch
  tgw_subnets                             = var.pub_workload_tgw_subnets
  s3_bucket_arn                           = module.create_s3.s3_bucket_arn
  tags                                    = var.tags
}

module "public_workload_vpc_public_subnets" {
  providers = {
    aws = aws.use1
  }
  for_each                = tomap(zipmap(keys(module.public_workload_vpc)[*], values(module.public_workload_vpc)[*].vpc_id))
  source                  = "git::https://bitbucket.org/asrivastav190/terraform-modules//public_workload_vpc/vpc/subnets"
  map_public_ip_on_launch = var.map_public_ip_on_launch
  vpc_id                  = each.value
  subnets                 = var.public_subnets
  subnet_name             = format("%s-pub-subnet",each.key)
  vpc_name                = each.key
  tags                    = var.tags
}

module "public_workload_vpc_public_shared_subnets" {
  providers = {
    aws = aws.use1
  }
  for_each                = tomap(zipmap(keys(module.public_workload_vpc)[*], values(module.public_workload_vpc)[*].vpc_id))
  source                  = "git::https://bitbucket.org/asrivastav190/terraform-modules//public_workload_vpc/vpc/subnets"
  map_public_ip_on_launch = var.map_public_ip_on_launch
  vpc_id                  = each.value
  subnets                 = var.shared_public_subnets
  subnet_name             = format("%s-pub-share",each.key)
  vpc_name                = each.key
  tags                    = var.tags
}

module "public_workload_vpc_private_shared_subnets" {
  providers = {
    aws = aws.use1
  }
  for_each             = tomap(zipmap(keys(module.public_workload_vpc)[*], values(module.public_workload_vpc)[*].vpc_id))
  source               = "git::https://bitbucket.org/asrivastav190/terraform-modules//public_workload_vpc/vpc/subnets"
  map_public_ip_on_launch = var.map_public_ip_on_launch
  vpc_id               = each.value
  subnets              = var.shared_private_subnets
  subnet_name          = format("%s-prv-share",each.key)
  vpc_name             = each.key
  tags                 = var.tags
}

module "public_workload_vpc_tgw_route_table" {
  providers = {
    aws = aws.use1
  }
  for_each             = tomap(zipmap(keys(module.public_workload_vpc)[*], values(module.public_workload_vpc)[*].vpc_id))
  source               = "git::https://bitbucket.org/asrivastav190/terraform-modules//public_workload_vpc/vpc/route-table"
  gateway_id           = module.create_transit_gateway_infra.transit_gateway_id
  vpc_id               = each.value
  vpc_name             = each.key
  subnet_count         = keys(var.pub_workload_tgw_subnets)
  rt_suffix            = "TGW"
  sub_suffix           = "TGW"
  tags                 = var.tags
}

module "public_workload_vpc_prv_share_route_table" {
  providers = {
    aws = aws.use1
  }
  for_each             = tomap(zipmap(keys(module.public_workload_vpc)[*], values(module.public_workload_vpc)[*].vpc_id))
  source               = "git::https://bitbucket.org/asrivastav190/terraform-modules//public_workload_vpc/vpc/route-table"
  gateway_id           = module.create_transit_gateway_infra.transit_gateway_id
  vpc_id               = each.value
  vpc_name             = each.key
  subnet_count         = keys(var.shared_private_subnets)
  rt_suffix            = "Share-private"
  sub_suffix           = "prv-share"
  tags                 = var.tags
}

module "public_workload_vpc_pub_share_route_table" {
  providers = {
    aws = aws.use1
  }
  for_each             = tomap(zipmap(keys(module.public_workload_vpc)[*], values(module.public_workload_vpc)[*].vpc_id))
  source               = "git::https://bitbucket.org/asrivastav190/terraform-modules//public_workload_vpc/vpc/route-table"
  gateway_id           = zipmap(keys(module.public_workload_vpc)[*],values(module.public_workload_vpc)[*].igw_id)
  vpc_id               = each.value
  vpc_name             = each.key
  subnet_count         = keys(var.shared_public_subnets)
  rt_suffix            = "Share-public"
  sub_suffix           = "pub-share"
  tags                 = var.tags
}

module "public_workload_vpc_pub_route_table" {
  providers = {
    aws = aws.use1
  }
  for_each             = tomap(zipmap(keys(module.public_workload_vpc)[*], values(module.public_workload_vpc)[*].vpc_id))
  source               = "git::https://bitbucket.org/asrivastav190/terraform-modules//public_workload_vpc/vpc/route-table"
  gateway_id           = zipmap(keys(module.public_workload_vpc)[*],values(module.public_workload_vpc)[*].igw_id)
  vpc_id               = each.value
  vpc_name             = each.key
  subnet_count         = keys(var.public_subnets)
  rt_suffix            = "pub"
  sub_suffix           = "pub-subnet"
  tags                 = var.tags
}

module "public_workload_vpc_tgw_attachment" {
  providers = {
    aws = aws.use1
  }
  for_each                                = tomap(zipmap(keys(module.public_workload_vpc)[*], values(module.public_workload_vpc)[*].vpc_id))
  source                                  = "git::https://bitbucket.org/asrivastav190/terraform-modules//public_workload_vpc/vpc/vpc-tgw-attachment"
  transit_gateway_id                      = module.create_transit_gateway_infra.transit_gateway_id
  vpc_id                                  = each.value
  vpc_name                                = each.key
  spoke_vpc_inspection_tgw_route_table_id = module.create_transit_gateway_infra.spoke_vpc_inspection_tgw_route_table_id
  firewall_subnet_tgw_route_table_id      = module.create_transit_gateway_infra.firewall_subnet_tgw_route_table_id
  tags                                    = var.tags
}

module "public_workload_vpc_ram" {
  providers = {
    aws = aws.use1
  }
  for_each             = tomap(zipmap(keys(module.public_workload_vpc)[*], values(module.public_workload_vpc)[*].vpc_id))
  source               = "git::https://bitbucket.org/asrivastav190/terraform-modules//public_workload_vpc/vpc/ram"
  vpc_id               = each.value
  share_subnets        = flatten([values(module.public_workload_vpc_private_shared_subnets)[*].subnet_arn , values(module.public_workload_vpc_public_shared_subnets)[*].subnet_arn])
  workload_account_id  = var.workload_account_id
  ram_name             = var.ram_name
  tags                 = var.tags
}