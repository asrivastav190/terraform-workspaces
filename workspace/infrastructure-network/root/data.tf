## Template file for S3 bucket policy
data "template_file" "s3_bucket_policy" {
  template = file("${path.module}/templates/bucket-policy.json")
  vars = {
    bucket_name        = module.create_s3.s3_bucket_id
    sandbox_account_id = var.sandbox_account_id #change account ID in variable file as per requirement
  }
}