variable "role_arn" {}

# ##Workload VPC VPC Name and CIDR definition
variable "spoke_vpc_param" {
  type = map(object({
    vpc_name = string
    cidr_ab = string
    flow_log_enable = bool
  }))
  default = {
    production_vpc = {
      cidr_ab = "10.166"
      vpc_name = "production_vpc"
      flow_log_enable = true
    },
    development_vpc = {
      cidr_ab = "10.165"
      vpc_name = "development_vpc"
      flow_log_enable = true
    }
    # research_vpc = {
    #   cidr_ab = "10.164"
    #   vpc_name = "research_vpc"
    #   flow_log_enable = true
    # },
    # integration_vpc = {
    #   cidr_ab = "10.167"
    #   vpc_name = "integration_vpc"
    #   flow_log_enable = true
    # }
    ### Add another VPC name and CIDR here that need to be created as spoke VPC design
  }
}

variable "tgw_subnets" {
  description = "A map of availability zone ids to subnet c.d CIDR blocks, which will be set up as subnets."
  type = map
  default = {
    "use1-az4" = "0.112/28" 
    "use1-az6" = "0.128/28"
    "use1-az1" = "0.144/28"
    "use1-az2" = "0.160/28"
  }
}

variable "prv_subnets" {
  description = "A map of availability zone ids to subnet c.d CIDR blocks, which will be set up as subnets."
  type = map
  default = {
    "use1-az4" = "7.0/24"
    "use1-az6" = "8.0/24"
    "use1-az1" = "9.0/24"
    "use1-az2" = "10.0/24"
  }
}

variable "workload_subnets" {
  description = "A map of availability zone ids to subnet c.d CIDR blocks, which will be set up as subnets."
  type = map
  default = {
    "use1-az4" = "17.0/24"
    "use1-az6" = "18.0/24"
    "use1-az1" = "19.0/24"
    "use1-az2" = "20.0/24"
  }
}

# # ## Variables declared for inspection-ingress-egress module
variable "inspection_ingress_egress_param" {
  type = map(object({
    vpc_name = string
    cidr_ab = string
    flow_log_enable = bool
  }))
  default = {
    inspection_vpc = {
      cidr_ab = "10.161"
      vpc_name = "inspection_vpc"
      flow_log_enable = false 
    },
    ingress_vpc = {
      cidr_ab = "10.162"
      vpc_name = "ingress_vpc"
      flow_log_enable = false
    },
    egress_vpc = {
      cidr_ab = "10.163"
      vpc_name = "egress_vpc"
      flow_log_enable = false
    }
  }
}

variable "inspection_ingress_egress_tgw_subnets" {
  description = "A map of availability zone ids to subnet c.d CIDR blocks, which will be set up as subnets."
  type = map
  default = {
    "use1-az4" = "0.112/28" 
    "use1-az6" = "0.128/28"
    "use1-az1" = "0.144/28"
    "use1-az2" = "0.160/28"
  }
}

variable "inspection_nfw_subnets" {
  description = "A map of availability zone ids to subnet c.d CIDR blocks, which will be set up as subnets."
  type = map
  default = {
    "use1-az4" = "10.0/24"
    "use1-az6" = "11.0/24"
    "use1-az1" = "12.0/24"
    "use1-az2" = "13.0/24"
  }
}

variable "ingress_egress_pub_subnets" {
  description = "A map of availability zone ids to subnet c.d CIDR blocks, which will be set up as subnets."
  type = map
  default = {
    "use1-az4" = "40.0/24"
    "use1-az6" = "41.0/24"
    "use1-az1" = "42.0/24"
    "use1-az2" = "43.0/24"
  }
}

###Transit Gateway variable declaration/definition
variable "transit_gateway_name" {
  type        = string
  description = "Name of Transit Gateway in each region"
  default     = "transit-gateway-use1"
}
variable "peer_transit_gateway_name" {
  type        = string
  description = "Peer transit gateway name in us-west-2 region"
  default     = "peer-transit-gateway-usw2"
}

# ### Variable declared  for s3

variable "app_name" {
  description = "Application Name"
  type        = string
  default     = "iso"
}

variable "app_env" {
  description = "Application Name"
  type        = string
  default     = "dev"
}

variable "aws_region" {
  description = "AWS region"
  default     = "us-east-1"
}

variable "attach_policy" {
  description = "Controls if S3 bucket should have bucket policy attached (set to `true` to use value of `policy` as bucket policy)"
  type        = bool
  default     = true
}

variable "sse_algorithm" {
  description = "The server-side encryption algorithm to use. Valid values are AES256 and aws:kms"
  default     = "AES256"
}

variable "force_destroy" {
  description = "A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error"
  default     = true
}

# # https://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html#canned-acl
variable "acl" {
  description = "Set canned ACL on bucket. Valid values are private, public-read, public-read-write, aws-exec-read, authenticated-read, bucket-owner-read, bucket-owner-full-control, log-delivery-write"
  default     = "private"
}


#PUT Bucket acl and PUT Object acl calls will fail if the specified ACL allows public access.
variable "block_public_acls" {
  description = "enable server access log of s3 bucket"
  type        = bool
  default     = true
}
#Ignore public ACLs on this bucket and any objects that it contains.
variable "ignore_public_acls" {
  description = "enable server access log of s3 bucket"
  type        = bool
  default     = true
}

# #Reject calls to PUT Bucket policy if the specified bucket policy allows public access.
variable "block_public_policy" {
  description = "enable server access log of s3 bucket"
  type        = bool
  default     = true
}

# #Only the bucket owner and AWS Services can access this buckets if it has a public policy.
variable "restrict_public_buckets" {
  description = "enable server access log of s3 bucket"
  type        = bool
  default     = true
}

variable "sandbox_account_id" {
  description = "account ID of sandbox account for bucket policy" #need to change account ID and variable name as per requirement
  default     = "419119075743"                                    #eg 419119075743
}

variable "versioning" {
  description = "Map containing versioning configuration." #S3 Versioning to keep multiple versions of an object in one bucket
  type        = bool
  default     = true
}

variable "bucket" {
  description = "Bucket name"
  default     = "iso-s3-bucket"
}

variable "log_bucket" {
  description = "log Bucket name"
  default     = "iso-serveraccesslogs-bucket"
}

variable "replica_log_bucket" {
  description = "replica log Bucket name"
  default     = "replica-iso-serveraccesslogs-bucket"
}

# # ## Variables declared for endpint module

variable "enable_s3_endpoint" {
  description = "Select option to enable/disable s3 endpoint"
  type        = bool
  default     = true
}

## Tags
variable "tags" {
  type = map(string)
  default = { "iso-environment" = "sandbox", "iso-contact" = "eg@iso-ne.com", "iso-deployment-artifact" = "cmdb" , "iso-activity-code" = "1234"}
}

# /*
# variable "endpoint_param" {
#   type = map(object({
#     endpoint_name = string
#   }))
#   default = {
#     ingress_endpoint = {
#       endpoint_name = "ingress_endpoint"
#     },
#     egress_endpoint = {
#       endpoint_name = "egress_endpoint"
#     },
#     inspection_endpoint = {
#       endpoint_name = "inspection_endpoint"
#     },
#     workload_production_endpoint = {
#       endpoint_name = "workload_production_endpoint"
#     },
#     workload_development_endpoint = {
#       endpoint_name = "workload_development_endpoint"
#     },
#     workload_research_endpoint = {
#       endpoint_name = "workload_research_endpoint"
#     },
#     workload_integration_endpoint = {
#       endpoint_name = "workload_integration_endpoint"
#     }

#     ### Add another endpoint name here that need to be created as endpoint design
#   }
# }
# */

# ### Variables required for public_private workload vpc module.

variable "pub_prv_mspoke_vpc_param" {
  type = map(object({
    vpc_name = string
    cidr_ab = string
    public_flow_log_enable = bool
  }))
  default = {
    production_vpc = {
      cidr_ab = "10.177"
      vpc_name = "public_production_vpc"
      public_flow_log_enable = true
    },
    development_vpc = {
      cidr_ab = "10.178"
      vpc_name = "public_development_vpc"
      public_flow_log_enable = true
    }
    # research_vpc = {
    #   vpc_cidr = "10.164.0.0/16"
    #   vpc_name = "research_vpc"
    # },
    # integration_vpc = {
    #   vpc_cidr = "10.167.0.0/16"
    #   vpc_name = "integration_vpc"
    # }
    ### Add another VPC name and CIDR here that need to be created as spoke VPC design
  }
}

variable "pub_workload_tgw_subnets" {
    description = "A map of availability zone ids to subnet c.d CIDR blocks, which will be set up as subnets."
    type = map
    default = {
    "use1-az4" = "0.112/28" 
    "use1-az6" = "0.128/28"
    "use1-az1" = "0.144/28"
    "use1-az2" = "0.160/28"
    } 
}

variable "public_subnets" {
  description = "A map of availability zone ids to subnet c.d CIDR blocks, which will be set up as subnets."
  type = map
  default = {
    "use1-az4" = "7.0/24"
    "use1-az6" = "8.0/24"
    "use1-az1" = "9.0/24"
    "use1-az2" = "10.0/24"
  }
}

variable "shared_public_subnets" {
  description = "A map of availability zone ids to subnet c.d CIDR blocks, which will be set up as subnets."
  type = map
  default = {
    "use1-az4" = "17.0/24"
    "use1-az6" = "18.0/24"
    "use1-az1" = "19.0/24"
    "use1-az2" = "20.0/24"
  }
}

variable "shared_private_subnets" {
  description = "A map of availability zone ids to subnet c.d CIDR blocks, which will be set up as subnets."
  type = map
  default = {
    "use1-az4" = "50.0/24"
    "use1-az6" = "51.0/24"
    "use1-az1" = "52.0/24"
    "use1-az2" = "53.0/24"
  }
}

variable "map_public_ip_on_launch" {
  type = bool
  default = false
}

variable "workload_account_id" {
  description = "Provide account id where resources needs to be shared."
  type        = string
  default     = "672495051411"
}

variable "ram_name" {
  description = "Resource Access Manager Name"
  type        = string
  default     = "Test"
}