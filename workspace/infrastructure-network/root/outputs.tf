# ## output from transit gateway module
output "transit_gateway_id" {
  value       = module.create_transit_gateway_infra.transit_gateway_id
  description = "EC2 Transit Gateway identifier"
}

output "firewall_subnet_tgw_route_table_id" {
  value       = module.create_transit_gateway_infra.firewall_subnet_tgw_route_table_id
  description = "Transit Gateway Route Table identifier for the Firewall Subnet TGW Route Table"
}

### Private workload VPC outputs -> create_workload_vpc module.

# VPC ID
output "workload_vpc_id" {
  value       = values(module.workload_vpc)[*].vpc_id
  description = "The ID of the workload VPC"
}

# Transit gateway ids
output "tgw_subnet_ids" {
  value       = values(module.workload_vpc)[*].tgw_subnet_ids
  description = "Subnet IDs from Workload VPC, transit gateway subnets"
}

#Private subnet ids
output "prv_subnet_ids" {
  value       = values(module.workload_vpc_prv_subnets)[*].subnet_ids
  description = "Subnet IDs from Workload VPC, transit gateway subnets"
}

# Route table ID
output "workload_vpc_rt_id" {
  value       = values(module.workload_vpc_route_table)[*].rt_id
  description = "Route Table Ids from Workload VPC"
}

### Inspection,Ingress and Egress VPC outputs -> inspection_ingress_egress_vpcs module.

# VPC ID
output "inspection_ingress_egress_vpcs_id" {
  value = values(module.inspection_ingress_egress_vpcs)[*].vpc_id
}

## Inspection,Ingress and Egress transit subnet ids.
output "inspection_ingress_egress_tgw_subnet_ids" {
  value = values(module.inspection_ingress_egress_vpcs)[*].prv_tgw_subnet_ids
}

## Ingress and Egress public subnet ids.
output "ingress_egress_public_subnet_ids" {
  value = values(module.ingress_egress_pub_subnets)[*].subnet_ids
}

## Inspection network firewall subnet ids.
output "inspection_nfw_subnet_ids" {
  value = values(module.inspection_ingress_egress_vpcs)[*].inspection_nfw_subnet_ids
}

## Route table
output "inspection_ingress_egress_tgw_rt_ids" {
  value = values(module.inspection_ingress_egress_route_tables)[*].prv_tgw_rt_ids
}

output "inspection_nfw_rt_id" {
  value = values(module.inspection_ingress_egress_route_tables)[*].inspection_nfw_rt_id
}

output "ingress_egress_public_rt_ids" {
  value = values(module.inspection_ingress_egress_route_tables)[*].pub_rt_id
}

# #out from S3 iso module

# # # The name of the bucket.
output "bucket_id" {
  description = "The name of the bucket"
  value       = module.create_s3.s3_bucket_id
}

# # # The ARN of the bucket. Will be of format arn:aws:s3:::bucketname
output "bucket_arn" {
  description = "The ARN of the bucket. Will be of format arn:aws:s3:::bucketname"
  value       = module.create_s3.s3_bucket_arn
}

# ## output from  endpoint module

output "s3_endpoint_id" {
  #value       = "${aws_vpc_endpoint.s3.*.id}"
  value       = module.endpoint.s3_endpoint_id
  description = "The ID of the s3 endpoint."
}

output "s3_endpoint_prefix_list_id" {
  #value       = "${aws_vpc_endpoint.s3.*.prefix_list_id}"
  value       = module.endpoint.s3_endpoint_prefix_list_id
  description = "The prefix list ID of the s3 endpoint."
}

# ## output from public workload vpc module
output "pub_workload_vpc_ids" {
  value = values(module.public_workload_vpc)[*].vpc_id
}

## Subnets
output "pub_workload_vpc_tgw_subnet_ids" {
  value = values(module.public_workload_vpc)[*].prv_tgw_subnet_ids
}

output "prv_share_subnet_ids" {
  value       = values(module.public_workload_vpc_private_shared_subnets)[*].subnet_ids
  description = "Sharing subnet IDs for the /24 subnet"
}

output "pub_share_subnet_ids" {
  value       = values(module.public_workload_vpc_public_shared_subnets)[*].subnet_ids
  description = "Sharing public subnet IDs for the /24 subnet"
}

output "pub_nfw_subnet_ids" {
  value       = values(module.public_workload_vpc_public_subnets)[*].subnet_ids
  description = "NFW public subnet IDs for the /24 subnet"
}

##Route table
output "prv_share_rt_id" {
  value       = values(module.public_workload_vpc_prv_share_route_table)[*].rt_id
  description = "Public workload VPC private share route table identifier"
}

output "prv_tgw_rt_id" {
  value       = values(module.public_workload_vpc_tgw_route_table)[*].rt_id
  description = "Public workload vpc private tgw route table identifier"
}

output "pub_vpce_rt_id" {
  value       = values(module.public_workload_vpc_pub_route_table)[*].rt_id
  description = "VPCE public RT"
}

# output "aws_ram_resource_id" {
#   value = values(module.ram)[*].aws_ram_resource_id
# }

output "ram_id" {
  value = values(module.public_workload_vpc_ram)[*].ram_id
}