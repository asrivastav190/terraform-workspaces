locals {
  inspection_ingress_egress_vpc_name_id_map = zipmap(keys(module.inspection_ingress_egress_vpcs)[*], values(module.inspection_ingress_egress_vpcs)[*].vpc_id)
  selected_values = matchkeys(values(module.inspection_ingress_egress_vpcs)[*].vpc_id,keys(module.inspection_ingress_egress_vpcs)[*],["ingress_vpc"])
  ingress_egress_vpcs_id = [
    for i,z in local.inspection_ingress_egress_vpc_name_id_map: z
    if i == "ingress_vpc" || i == "egress_vpc"
  ]
  ingress_egress_vpcs_name = [
    for i,z in local.inspection_ingress_egress_vpc_name_id_map : i
    if i == "ingress_vpc" || i == "egress_vpc"
  ]
  inspection_vpc_id =[
    for i,z in local.inspection_ingress_egress_vpc_name_id_map : z
    if i == "inspection_vpc"
  ]
  ingress_vpc_id =[
    for i,z in local.inspection_ingress_egress_vpc_name_id_map : z
    if i == "ingress_vpc"
  ]

  egress_vpc_id =[
    for i,z in local.inspection_ingress_egress_vpc_name_id_map : z
    if i == "egress_vpc"
  ]
}