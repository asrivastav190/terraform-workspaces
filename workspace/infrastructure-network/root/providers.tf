provider "aws" {
  alias  = "usw2"
  region = "us-west-2"
}

provider "aws" {
  alias  = "use1"
  region = "us-east-1"
  profile = "default"
}

provider "aws" {
  alias  = "pwc_explore"
  region = "us-east-1"
  profile = "pwc_explore"
  assume_role {
    role_arn     = var.role
  }
}
