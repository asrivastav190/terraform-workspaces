terraform {
  required_providers {
    aws = {
      source                = "hashicorp/aws"
      version               = "3.73.0"
      configuration_aliases = [aws.usw2, aws.use1, aws.pwc_explore]
    }
  }
}